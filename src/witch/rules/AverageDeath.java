package witch.rules;

public interface AverageDeath extends People{
	int peopleKilledOnYears(int num);
	Double averagePeopleKilled();
}
