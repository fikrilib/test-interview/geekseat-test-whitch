package witch.rules;

public interface People {
	int getAgeOfDeath(int num);
	int getYearOfDeath(int num);
	int getBornOnYear(int num);
	
}
