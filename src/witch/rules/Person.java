package witch.rules;

public interface Person {
	int ruleKiller();
    int peopleKilled1(int yearOfDeath, int ageOfDeath);
    int peopleKilled2(int yearOfDeath, int ageOfDeath);
    int averageKilled();
}
